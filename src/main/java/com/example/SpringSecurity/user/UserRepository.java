package com.example.SpringSecurity.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserApp, Long> {
    UserApp findByUsername(String username);
    boolean existsByUsername(String username);
}
