package com.example.SpringSecurity.user;

import com.example.SpringSecurity.payload.LoginRequest;

import java.util.List;

public interface IUserService {

    UserApp createUser(LoginRequest loginRequest);
    List<?> listUser();
    void deleteUser(Long id);
    UserApp updateUser(Long id, UserApp userApp);
}
