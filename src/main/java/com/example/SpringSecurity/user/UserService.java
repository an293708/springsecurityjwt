package com.example.SpringSecurity.user;

import ch.qos.logback.core.spi.ErrorCodes;
import com.example.SpringSecurity.enums.Role;
import com.example.SpringSecurity.exception.AppException;
import com.example.SpringSecurity.exception.ErrorCode;
import com.example.SpringSecurity.jwt.JwtToken;
import com.example.SpringSecurity.payload.LoginRequest;
import org.apache.catalina.User;
import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;


@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private JwtToken jwtToken;

    @Override
    public UserApp createUser(LoginRequest loginRequest) {
        if (userRepository.existsByUsername(loginRequest.getUsername()))
            throw new AppException(ErrorCode.USER_EXISTED);

        HashSet<String> roles = new HashSet<>();
        roles.add(Role.USER.name());

        UserApp userApp = new UserApp();
        userApp.setUsername(loginRequest.getUsername());
        userApp.setPassword(loginRequest.getPassword());
        userApp.setRoles(roles);
        return userRepository.save(userApp);
    }

    @Override
    public List<?> listUser() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserApp updateUser(Long id, UserApp user) {
        UserApp userApp = userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User Not Found")
        );
        userApp.setUsername(user.getUsername());
        userApp.setPassword(user.getPassword());
        userApp.setRoles(user.getRoles());

        return userRepository.save(userApp);
    }

}
