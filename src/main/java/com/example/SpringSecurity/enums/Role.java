package com.example.SpringSecurity.enums;

import lombok.Getter;

public enum Role {
    ADMIN,
    STAFF,
    USER
}
