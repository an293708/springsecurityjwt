package com.example.SpringSecurity;

import com.example.SpringSecurity.jwt.JwtToken;
import com.example.SpringSecurity.payload.LoginRequest;
import com.example.SpringSecurity.payload.LoginResponse;
import com.example.SpringSecurity.user.UserApp;
import com.example.SpringSecurity.user.UserRepository;
import com.example.SpringSecurity.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class Controller {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private UserService userService;

    @PostMapping("login")
    public LoginResponse authenticateUser(@RequestBody LoginRequest loginRequest) throws Exception {
        UserApp userApp = userRepository.findByUsername(loginRequest.getUsername());
        if(userApp != null && loginRequest.getPassword().equals(userApp.getPassword())){
            String jwt = jwtToken.generateToken(loginRequest);
            return new LoginResponse(jwt);
        }else{
            throw new Exception("Authentication failed");
        }
    }

    @PutMapping("delete")
    public void deleteUser(Long id){
        userService.deleteUser(id);
    }

    @PutMapping("create")
    public UserApp createUser(@RequestBody LoginRequest loginRequest){
        UserApp userApp = userService.createUser(loginRequest);
        return userApp;
    }

    @PutMapping("update")
    public UserApp updateUser(@RequestBody UserApp userApp){
        return userService.updateUser(userApp.getId(),userApp);
    }
    @GetMapping("test")
    public String test(){
        return "Hello";
    }
}
