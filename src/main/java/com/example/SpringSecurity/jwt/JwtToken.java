package com.example.SpringSecurity.jwt;

import com.example.SpringSecurity.payload.LoginRequest;
import com.example.SpringSecurity.user.UserApp;
import com.example.SpringSecurity.user.UserRepository;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.StringJoiner;


@Component
@Slf4j
public class JwtToken {

    @Autowired
    private UserRepository userRepository;
    private final String SECRET_KEY = "21324";
    private final long EXPIRATION_TIME = 604800000L;

    public String generateToken(LoginRequest loginRequest){
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + EXPIRATION_TIME);
        UserApp userApp = userRepository.findByUsername(loginRequest.getUsername());
        JWSHeader jwsHeader = new JWSHeader(JWSAlgorithm.HS512);
        JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
                .subject(userApp.getUsername())
                .issueTime(now)
                .expirationTime(expiryDate)
                .claim("scope",buildScope(userApp))
                .build();
        Payload payload = new Payload(jwtClaimsSet.toJSONObject());
        JWSObject jwsObject = new JWSObject(jwsHeader,payload);
        var key = TextCodec.BASE64.decode(SECRET_KEY);
        var paddedKey = key.length < 128 ? Arrays.copyOf(key, 128) : key;
        try {
            jwsObject.sign(new MACSigner(paddedKey));
            return jwsObject.serialize();
        } catch (JOSEException e) {
            log.error("Cannot create jwt");
            throw new RuntimeException(e);
        }
    }

    public String buildScope(UserApp userApp){
        StringJoiner stringJoiner = new StringJoiner(" ");
        if(!CollectionUtils.isEmpty(userApp.getRoles()))
            userApp.getRoles().forEach(stringJoiner::add);
        return stringJoiner.toString();
    }

    public String getUsernameFromJWT(String token){
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

    public boolean validateToken(String authToken) throws JOSEException, ParseException {
        var key = TextCodec.BASE64.decode(SECRET_KEY);
        var paddedKey = key.length < 128 ? Arrays.copyOf(key, 128) : key;
        JWSVerifier verifier = new MACVerifier(paddedKey);

        SignedJWT signedJWT = SignedJWT.parse(authToken);

        Date expiryTime = signedJWT.getJWTClaimsSet().getExpirationTime();

        return signedJWT.verify(verifier) && expiryTime.after(new Date());
    }

}
